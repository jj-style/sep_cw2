import unittest
import os
from app import app, models, db
import sqlite3

__unittest = True

class Tests(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(os.path.abspath(os.path.dirname(__file__)),
        'test.db')
        db.drop_all()
        db.create_all()

        self.m1 = models.Module()
        self.m1.m_id = "COMP1111"
        self.m1.m_name = "Module Name"

        self.m1_c = models.Module()
        self.m1_c.m_id = "COMP1111"
        self.m1_c.m_name = "Module Name"

        self.m2 = models.Module()
        self.m2.m_id = "COMP2222"
        self.m2.m_name = "Module Name Different"

        self.u1 = models.User()
        self.u1.name = "user 1"
        self.u1.password = "password"
        self.u1.u_type = False

        self.u2 = models.User()
        self.u2.name = "user 2"
        self.u2.password = "password"
        self.u2.u_type = True

        self.app = app.test_client()
        self.app.testing = True 

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_database_insert_valid(self):
        db.session.add(self.m1)
        db.session.commit()
        self.assertTrue(True)
    

    @unittest.expectedFailure
    def test_database_insert_invalid(self):
        db.session.add(self.m1)
        db.session.add(self.m1_c)
        db.session.commit()
        self.assertTrue(False)

    def test_add_user_student(self):
        db.session.add(self.u1)
        db.session.commit()
        user = models.User.query.first()
        s = models.Student()
        s.s_id = user.u_id
        self.assertTrue(True)

    def test_add_student_parent_relationship(self):
        db.session.add(self.u1)
        db.session.add(self.u2)
        db.session.commit()
        users = models.User.query.all()
        user1, user2 = users[0], users[1]
        user1.relationship = user2.u_id
        user2.relationship = user1.u_id
        db.session.commit()
        users = models.User.query.all()
        user1, user2 = users[0], users[1]
        self.assertEqual(models.User.query.get(user1.relationship),user2)
        self.assertEqual(models.User.query.get(user2.relationship),user1)

    def test_home_page(self):
        result = self.app.get('/')
        self.assertEqual(result.status_code, 200)

    
    def test_student_page(self):
        db.session.add(self.u1)
        db.session.commit()
        user = models.User.query.first()
        with self.app.session_transaction() as session:
            session['user'] = user.u_id
            session['utype'] = user.u_type
        result = self.app.get('/indexStudent')
        self.assertEqual(result.status_code, 200)

    def test_parent_page(self):
        db.session.add(self.u1)
        user = models.User.query.first()
        s = models.Student()
        s.s_id = user.u_id
        db.session.add(s)
        db.session.add(self.u2)
        db.session.commit()
        users = models.User.query.all()
        user1, user2 = users[0], users[1]
        user1.relationship = user2.u_id
        user2.relationship = user1.u_id
        db.session.commit()
        user = models.User.query.get(1)
        with self.app.session_transaction() as session:
            session['user'] = user.u_id
            session['utype'] = user.u_type
        result = self.app.get('/indexParent')
        self.assertEqual(result.status_code, 200)

    def test_parent_attendance_page(self):
        db.session.add(self.u1)
        user = models.User.query.first()
        s = models.Student()
        s.s_id = user.u_id
        db.session.add(s)
        db.session.add(self.u2)
        db.session.commit()
        users = models.User.query.all()
        user1, user2 = users[0], users[1]
        user1.relationship = user2.u_id
        user2.relationship = user1.u_id
        db.session.commit()
        user = models.User.query.get(1)
        with self.app.session_transaction() as session:
            session['user'] = user.u_id
            session['utype'] = user.u_type
        result = self.app.get('/attendance')
        self.assertEqual(result.status_code, 200)

    @unittest.expectedFailure
    def test_logout_page(self):
        db.session.add(self.u1)
        db.session.add(self.u2)
        with self.app.session_transaction() as session:
            session['user'] = user.u_id
            session['utype'] = user.u_type
        result = self.app.get('/logout')
        self.assertEqual(result.status_code, 200)
        with self.app.session_transaction() as session:
            temp = session['user']
            temp = session['utype']
        self.assertTrue(False)
        
if __name__ == "__main__":
    unittest.main()
