from flask import render_template, flash, redirect, url_for, request, session
from app import app, models, db, bcrypt
from .forms import Login, Signup
import os, json
from datetime import datetime

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    login = Login()
    signup = Signup()
    if request.method == 'POST':
        if 'login' in request.form:
            user = models.User.query.get(login.uid.data)
            if bcrypt.check_password_hash(user.password, login.password.data):
                session['user'] = user.u_id
                session['utype'] = user.u_type
                if user.u_type == True:
                    return redirect(url_for('indexParent'))
                else:
                    return redirect(url_for('indexStudent'))
            else:
                print("password incorrect")

        elif 'signup' in request.form:
            try:
                d = {"True":True, "False":False}
                new_user = models.User()
                new_user.name = signup.name.data
                new_user.password = bcrypt.generate_password_hash(signup.password.data).decode('utf-8')
                new_user.u_type = d[signup.user_type.data]
                db.session.add(new_user)
                db.session.commit()
                user = models.User.query.filter(models.User.name==signup.name.data).first()
                flash("Your user_id is {}".format(user.u_id))
                if not user.u_type:
                    new_student = models.Student()
                    new_student.s_id = user.u_id
                    db.session.add(new_student)
                    db.session.commit()
                return redirect(url_for('index'))
            except Exception as e:
                flash("error creating new user")
                flash(e.args[0])
    return render_template("index.html", title="SNOOP", f1=login, f2=signup)

@app.route('/indexParent', methods=['GET'])
def indexParent():
    user = models.User.query.get(session['user'])
    child = models.User.query.get(user.relationship)
    try:
        location = models.Location.query.filter(models.Location.s_id==child.u_id).order_by(models.Location.date_time.desc()).first()
        if location == None:
            location = {"latitude":"no location data","longitude":"no location data", "date_time":datetime.now()}
    except:
        location = {"latitude":"no location data","longitude":"no location data", "date_time":datetime.now()}
    try:
        images = models.Photos.query.filter(models.Photos.s_id==user.relationship).order_by(models.Photos.date_time.desc()).all()
    except Exception as e:
        print(e.args[0])
        images = []
    return render_template("index_parent.html", title="SNOOP | Parent",
    name=user.name, child=child, location=location, images=images)

@app.route('/attendance', methods=['GET'])
def attendance():
    s_id = models.User.query.get(session['user']).relationship
    s_name = models.User.query.get(s_id).name
    s_attendance = models.Association.query.filter(models.Association.s_id==s_id).all()
    return render_template('attendance.html', 
                            title="SNOOP | Attendance",
                            name=s_name,
                            attendance=s_attendance)

@app.route('/indexStudent', methods=['GET','POST'])
def indexStudent():
    user = models.User.query.get(session['user'])
    if request.method == 'POST':
      f = request.files['file']
      now = datetime.now()
      now_string = datetime.strftime(now,"%d.%m.%y_%H.%M.%S")
      img = models.Photos()
      img.s_id = session['user']
      img.date_time = now
      img.photo = now_string
      f.save(os.path.join(app.config['UPLOAD_DIR'],str(user.u_id) + "_" + now_string+".jpg"))
      db.session.add(img)
      db.session.commit()
      flash("File uploaded successfully")
    return render_template("index_student.html", title="SNOOP | Student", name=user.name)

@app.route('/postLocation', methods=['GET','POST'])
def postLocation():
    data = json.loads(request.data)
    latitude = data.get('lat')
    longitude = data.get('long')
    new_location = models.Location()
    new_location.s_id = session['user']
    new_location.latitude = latitude
    new_location.longitude = longitude
    new_location.date_time = datetime.now()
    db.session.add(new_location)
    db.session.commit()
    return redirect(request.referrer)

@app.route('/logout', methods=['GET'])
def logout():
    session.pop('user',None)
    session.pop('utype',None)
    return redirect(url_for('index'))

