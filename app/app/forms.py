from flask_wtf import FlaskForm as Form
from wtforms import IntegerField, RadioField, StringField, PasswordField
from wtforms.validators import DataRequired

class Login(Form):
    uid = IntegerField("User ID", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])

class Signup(Form):
    name = StringField("Name", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    user_type = RadioField("Student or Parent", choices=[(True,'Parent'),(False,'Student')])