import os

#security stuff
WTF_CSRF_ENABLED = True
SECRET_KEY = 'this is spartaaaaaaaaa'

#database stuff
basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True  

#file upload stuff
#UPLOAD_DIR = os.path.join(basedir,'app/upload_dir')
UPLOAD_DIR = os.path.join(basedir,'app/static/upload_dir')