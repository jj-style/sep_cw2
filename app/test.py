from app import app, models, db, bcrypt
import random

MODULES = {"COMP2421":"Numerical Computation",
           "COMP2211":"Operating Systems",
           "COMP2811":"User Interfaces",
           "COMP2711":"Algorithms 1",
           "COMP2912":"Software Engineering Principles",
           "COMP2011":"Web Application Development"}

def createModules():
    for code, name in MODULES.items():
        try:
            new_module = models.Module()
            new_module.m_id = code
            new_module.m_name = name
            db.session.add(new_module)
        except Exception as e:
            print("Error adding {}:{} to the database".format(code,name))
            print(e.args[0])

    try:
        db.session.commit()
    except Exception as e:
        print(e.args[0])

def createUser(uid, name, pwd, utype):
    u = models.User()
    u.u_id = uid
    u.name = name
    u.password = bcrypt.generate_password_hash(pwd).decode('utf-8')
    u.u_type = utype
    db.session.add(u)
    db.session.commit()
    
def createStudent(user):
    s = models.Student()
    s.s_id = user.u_id
    db.session.add(s)
    db.session.commit()

def enrol(user):
    for module in models.Module.query.all():
        module.students.append(user)
    db.session.commit()
        
def randomAttendance():
    for a in models.Association.query.all():
        a.attendance = random.randint(75,100)
    db.session.commit()

def createRelationship(id_1, id_2):
    user1 = models.User.query.get(id_1)
    user2 = models.User.query.get(id_2)
    user1.relationship = id_2
    user2.relationship = id_1
    db.session.commit()

if __name__ == "__main__":
##    createModules()
##    createUser("201229011", "Joseph Style", "password", False)
##    createUser("0", "Parent Style", "password", True)
##    createStudent(models.User.query.get(201229011))
##    enrol(models.Student.query.first())
##    randomAttendance()
##    createRelationship("201229011","0")
    
    #ss = models.Student.query.all()
    #ms = models.Module.query.all()
    #ass = models.Association.query.all()
    #us = models.Users.query.all()
    pass
