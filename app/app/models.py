from app import db
from datetime import datetime

class Association(db.Model):
    __tablename__ = 'association'
    s_id = db.Column(db.Integer, db.ForeignKey('student.s_id'), primary_key=True, nullable=False)
    m_id = db.Column(db.String(10), db.ForeignKey('module.m_id'), primary_key=True, nullable=False)
    
    attendance = db.Column(db.Float)

    student = db.relationship("Student", backref=db.backref("association", cascade="all, delete-orphan"))
    module = db.relationship("Module", backref=db.backref("association", cascade="all, delete-orphan"))

class Student(db.Model):
    __tablename__ = 'student'
    s_id = db.Column(db.Integer, db.ForeignKey('user.u_id'), primary_key=True)
    modules = db.relationship("Module", secondary="association")

class Module(db.Model):
    __tablename__ = 'module'
    m_id = db.Column(db.String(10), nullable=False, primary_key=True)
    m_name = db.Column(db.String(32), unique=True)
    students = db.relationship("Student", secondary="association")

class User(db.Model):
    __tablename__ = 'user'
    u_id = db.Column(db.Integer, primary_key=True)
    relationship = db.Column(db.Integer, db.ForeignKey('user.u_id'))
    name = db.Column(db.String(32))
    password = db.Column(db.String(60))
    u_type = db.Column(db.Boolean)

class Photos(db.Model):
    __tablename__ = 'photos'
    s_id = db.Column(db.Integer, db.ForeignKey('user.u_id'), primary_key=True)
    photo = db.Column(db.String(128))
    date_time = db.Column(db.DateTime,
        default=datetime.now(), primary_key=True)

class Location(db.Model):
    __tablename__ = 'location'
    s_id = db.Column(db.Integer, db.ForeignKey('user.u_id'), primary_key=True)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    date_time = db.Column(db.DateTime,
        default=datetime.now(), primary_key=True)