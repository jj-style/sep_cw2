var x = document.getElementById("location");
var map_btn = document.getElementById("map-btn");

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.watchPosition(showPosition);
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}
function showPosition(position) {
  x.innerHTML = "<p>Latitude: " + position.coords.latitude + "</p>" +
    "<p>Longitude: " + position.coords.longitude + "</p>";
    var obj = {lat: position.coords.latitude, long: position.coords.longitude}
  $.ajax({
    type: "POST",
    url: "postLocation",
    data: JSON.stringify(obj),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (response) {
      console.log("yay");
    }
  });
}
window.onload = getLocation();

